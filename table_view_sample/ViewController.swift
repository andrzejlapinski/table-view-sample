//
//  ViewController.swift
//  table_view_sample
//
//  Created by Andrzej Lapinski on 18/11/2018.
//  Copyright © 2018 Andrzej Lapinski. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell()
        print("👽Calling cellForRowAtIndexPath for row: \(indexPath.row)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath)
        cell.textLabel?.text = "This is the cell for \(indexPath.row)"
        
        cell.imageView?.image = UIImage(named: "Toast")
        cell.accessoryType = .disclosureIndicator
        cell.detailTextLabel?.text = "This is some detail text"
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

